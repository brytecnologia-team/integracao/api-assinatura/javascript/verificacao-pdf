# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 
Empty

## [1.0.0] - 2020-03-20

### Added

- Examples of signature verification. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/verificacao-pdf/-/tags/1.0.0
