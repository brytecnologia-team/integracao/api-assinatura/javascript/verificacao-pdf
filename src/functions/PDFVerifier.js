const request = require('request');
const serviceConfig = require('../config/ServiceConfig');
const signatureConfig = require('../config/SignatureConfig');

var fs = require('fs');

const URL_SERVER_PDF = serviceConfig.URL_PDF_VERIFIER;

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

const configuredToken = () => {
    if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
        console.log('Set up a valid token');
        return false;
    }
    return true;
}


const verifyPDFSignature = (URL_SERVER_PDF) => {

    const verificationForm = {
        'nonce': signatureConfig.NONCE,
        'signatures[0][nonce]': signatureConfig.NONCE_OF_SIGNATURE,
        'signatures[0][content]': fs.createReadStream(signatureConfig.SIGNATURE_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_SERVER_PDF, formData: verificationForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

module.exports = { URL_SERVER_PDF, verifyPDFSignature, configuredToken };