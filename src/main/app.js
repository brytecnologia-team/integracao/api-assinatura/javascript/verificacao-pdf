const { URL_SERVER_PDF, verifyPDFSignature, configuredToken } = require('../functions/PDFVerifier');

const verificationReport = (result) => {

    console.log('verification JSON response: ', result);

    var numberOfVerifiedSignatures = result.verificationStatus.length;

    console.log();

    console.log('Number of verified signatures: ', numberOfVerifiedSignatures);

    for (var indexOfSignatureList = 0; indexOfSignatureList < numberOfVerifiedSignatures; indexOfSignatureList++) {

        var verifiedSignature = result.verificationStatus[indexOfSignatureList];

        console.log();
        console.log('General state: ', verifiedSignature.generalStatus);

        console.log('Signature standard: ', verifiedSignature.signatureFormat);

        var statusOfVerifiedSignature = verifiedSignature.signatureStatus[0];

        if (statusOfVerifiedSignature.signaturePolicyStatus != undefined) {
            console.log('Signature profile: ', statusOfVerifiedSignature.signaturePolicyStatus.profile);
        }

        console.log('Signature state: ', statusOfVerifiedSignature.status);

        console.log('Base64 of the original document hash: ', statusOfVerifiedSignature.originalFileBase64Hash);

        console.log('Signature date: ', statusOfVerifiedSignature.signingTime);

        console.log('Verification reference type: ', statusOfVerifiedSignature.verificationReferenceType);

        console.log();

        var statusOfSignatureTimeStamp = statusOfVerifiedSignature.signatureTimeStampStatus;

        if (statusOfSignatureTimeStamp != undefined && statusOfSignatureTimeStamp.length > 0) {
            console.log('Time stamp information: ');
            const indexOfTimeStampCertificate = statusOfSignatureTimeStamp[0].timestampChainStatus.certificateStatusList.length - 1;
            const timeStampServerCertificate = statusOfSignatureTimeStamp[0].timestampChainStatus.certificateStatusList[indexOfTimeStampCertificate];
            console.log('Time stamp server: ', timeStampServerCertificate ? timeStampServerCertificate.certificateInfo.subjectDN.cn : ' ');
            console.log('Time stamp status: ', statusOfSignatureTimeStamp[0].status);
            console.log('Time stamp date: ', statusOfSignatureTimeStamp[0].timeStampDate);
            console.log('Verification reference date: ', statusOfSignatureTimeStamp[0].verificationReference);
            console.log('Verification reference type: ', statusOfSignatureTimeStamp[0].verificationReferenceType);
            console.log('Time stamp policy: ', statusOfSignatureTimeStamp[0].timeStampPolicy);
            console.log('Hash of time stamp content: ', statusOfSignatureTimeStamp[0].timeStampContentHash);
            console.log('Serial number of the time stamp: ', statusOfSignatureTimeStamp[0].timestampSerialNumber);
            console.log();
        }

        var statusOfSignatoryChain = statusOfVerifiedSignature.chainStatus;
        const indexOfSignatoryCertificate = statusOfSignatoryChain.certificateStatusList.length - 1;
        const signatoryCertificate = statusOfSignatoryChain.certificateStatusList[indexOfSignatoryCertificate];

        console.log('Signatory information: ')
        if (statusOfSignatoryChain != undefined && signatoryCertificate != undefined) {
            console.log('Signatory: ', signatoryCertificate.certificateInfo.subjectDN.cn);
            console.log('General status of the certificate chain: ', statusOfSignatoryChain.status);
            console.log('Signer certificate status: ', signatoryCertificate.status);
            console.log('ICP-Brazil certificate: ', signatoryCertificate.pkiBrazil);
            console.log('Initial validity date of the signer\'s certificate: ', signatoryCertificate.certificateInfo.validity.notBefore);
            console.log('End date of validity of the signatory\'s certificate: ', signatoryCertificate.certificateInfo.validity.notAfter);

        } else {
            console.log('Incomplete chain of the signer: It was not possible to verify the signer\'s certificate');
            console.log('General status of the signer\'s chain: ', statusOfSignatoryChain.status);
        }

    }

}

async function main() {

    if (configuredToken()) {

        console.log('Starting PDF / PAdES signature verification...');

        verifyPDFSignature(URL_SERVER_PDF).then(result => {

            verificationReport(result);

        }).catch(error => {
            console.log(error);
        })

    }

}

main();