module.exports = {

    URL_PDF_VERIFIER: 'https://fw2.bry.com.br/api/pdf-verification-service/v1/signatures/verify',

    ACCESS_TOKEN: '<INSERT_VALID_ACCESS_TOKEN>'
}