# Verificação de Assinatura PDF

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia JavaScript para verificação de assinatura PDF. 

### Tech

O exemplo utiliza a biblioteca JavaScript abaixo:
* [Request] - Bibliotecas para fazer chamadas http.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.


**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a verificação da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig

### Uso

   [Request]: <https://github.com/request/request>


<b>1</b> - As dependências podem ser instaladas utilizando o gerenciador de pacotes npm ou yarn.

Na pasta raiz do projeto, execute o comando de instalação conforme o gerenciador de dependências de sua preferência:

Instalando as dependências com npm:

    npm install

Instalando as dependências com yarn:

    yarn

<b>2</b> - Para executar a aplicação execute o comando:

    node src/main/app.js


